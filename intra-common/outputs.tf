output "tf_default_id" {
    value = "${aws_security_group.tf_default.id}"
}

output "tf_default_name" {
    value = "${aws_security_group.tf_default.name}"
}
