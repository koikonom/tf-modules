provider "aws" {
    access_key = "${var.access_key}"
    secret_key = "${var.secret_key}"
    region = "${var.region}"
}

resource "aws_security_group" "tf_default" {
  name = "tf_default"
  description = "tf_default"

  ingress {
      from_port = 22
      to_port = 22
      protocol = "tcp"
      cidr_blocks = ["${var.my_ip}"]
  }

  tags {
    Name = "tf_default"
  }
}
