variable access_key { }
variable secret_key { }
variable region { default = "eu-west-1" }
variable my_ip { default = "0.0.0.0/0" }
