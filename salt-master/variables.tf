variable access_key { }
variable secret_key { }
variable region { default = "eu-west-1" }
variable "amis" {
    default = {
        eu-west-1 = "ami-fd6cbd8a"
    }
}
variable instance_type { default = "t2.micro" }
variable key_name { default = "default" }
variable provisioning_key { default = "../config/default.pem" }
variable my_ip { default = "0.0.0.0/0" }
variable access_sg_name {}
variable access_sg_id {}
