output "master_id" {
    value = "${aws_instance.master.id}"
}

output "master_ip" {
    value = "${aws_eip.master_ip.public_ip}"
}

output "master_prv_ip" {
    value = "${aws_eip.master_ip.private_ip}"
}
