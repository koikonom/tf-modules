
provider "aws" {
    access_key = "${var.access_key}"
    secret_key = "${var.secret_key}"
    region = "${var.region}"
}

resource "aws_security_group" "salt_master_sg" {
    name = "tf_salt_master"
    description = "tf_salt_master"

    ingress {
        from_port = 4505
        to_port = 4506
        protocol = "tcp"
        security_groups = ["${var.access_sg_id}"]
    }

    ingress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = ["${var.my_ip}"]
    }

    tags {
        Name = "tf_salt_master"
    }

}

resource "aws_instance" "master" {
    ami = "${lookup(var.amis, var.region)}"
    instance_type = "${var.instance_type}"
    security_groups = [ 
                        "${aws_security_group.salt_master_sg.name}",
                        "${module.intra-common.tf_default_name}"
                      ]
    tags {
        Name = "tf_salt_master"
    }
    key_name = "${var.key_name}"

    connection {
        user = "ubuntu"
        key_file = "${var.provisioning_key}"
    }

    provisioner  "file" {
        source = "${path.module}/files/bootstrap-salt.sh"
        destination = "/tmp/bootstrap-salt.sh"
    }

    provisioner "remote-exec" {
        inline = [
                    "sudo bash /tmp/bootstrap-salt.sh -M -A localhost -i tf_master git v2014.1.4",
                    "sleep 10s",
                    "sudo salt-key -y -a tf_master",
                    "sudo restart salt-minion"
                ]
    }
}

resource "aws_eip" "master_ip" {
    instance = "${aws_instance.master.id}"
}


